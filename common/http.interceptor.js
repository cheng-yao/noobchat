const install = (Vue, vm) => {
    // 此为自定义配置参数，具体参数见上方说明
    Vue.prototype.$u.http.setConfig({
        method: 'POST',
        baseUrl: 'https://api.bbbug.com/api',
        dataType: 'json', // 设置为json，返回后会对数据进行一次JSON.parse()
        loadingText: '努力加载中~', //请求loading中的文字提示
        showLoading: true, // 是否显示请求中的loading
        loadingTime: 500, // 在此时间内，请求还没回来的话，就显示加载中动画，单位ms
        loadingMask: true, // 展示loading的时候，是否给一个透明的蒙层，防止触摸穿透
    });
    // 请求拦截部分，如配置，每次请求前都会执行
    Vue.prototype.$u.http.interceptor.request = (config) => {
        // 公共参数
        config.data.plat = 'app';
        config.data.version = 10000;
        // 引用token
        const token = uni.getStorageSync('token');
        config.data.access_token = token;

        if (!config.data.access_token) config.data.access_token = '45af3cfe44942c956e026d5fd58f0feffbd3a237';
        

        // 最后需要将config进行return
        return config;
        // 如果return一个false值，则会取消本次请求
        // if(config.url == '/user/rest') return false; // 取消某次请求
    }
}

export default {
    install
}
